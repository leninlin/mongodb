'use strict';

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

let config = {
  host: 'localhost',
  port: 27017,
  db: 'test'
};

const Mongodb = {
  configure(conf) {
    let {
      host = config.host,
      port = config.port,
      db = config.db
    } = conf;

    config = {host, port, db};
  },

  connect: () => {
    return new Promise((resolve, reject) =>
      MongoClient.connect(`mongodb://${config.host}:${config.port}/${config.db}`,
        (err, db) => err ? reject(err) : resolve(db)))
    .then(db => Mongodb.DB = db);
  },

  find: (collection, id) => {
    return new Promise((resolve, reject) =>
      Mongodb.DB.collection(collection)
        .findOne({_id: ObjectId(id)}, (err, result) => err ? reject(err) : resolve(result)));
  },

  findAll: (collection, params) => {
    return new Promise((resolve, reject) =>
      Mongodb.DB.collection(collection)
        .find(params).toArray((err, result) => err ? reject(err) : resolve(result)));
  },

  insert: (collection, data) => {
    return new Promise((resolve, reject) =>
      Mongodb.DB.collection(collection)
        .insertOne(data, (err, result) => err ? reject(err) : resolve(result.insertedId)));
  },

  update: (collection, id, data) => {
    return new Promise((resolve, reject) =>
      Mongodb.DB.collection(collection)
        .update({_id: ObjectId(id)}, data, (err, result) => err ? reject(err) : resolve(ObjectId(id))));
  },

  remove: (collection, filter) => {
    return new Promise((resolve, reject) =>
      Mongodb.DB.collection(collection)
        .remove(filter, (err, result) => err ? reject(err) : resolve()));
  },
}

module.exports = Mongodb;
